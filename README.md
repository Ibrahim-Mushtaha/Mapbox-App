<img width="365" alt="Mapbox" src="https://raw.githubusercontent.com/mapbox/mapbox-gl-js-docs/publisher-production/docs/pages/assets/logo.png" style="max-width:100%;">

When your users want to get from one location to another, don’t push them out of your application into a generic map application. Instead, keep them engaged with your application 100% of the time with in-app turn-by-turn navigation.

The Mapbox Navigation SDK for Android is built on top of the Mapbox Directions API and contains logic needed to get timed navigation instructions.

The Mapbox Navigation SDK is a precise and flexible platform which enables your users to explore the world's streets. We are designing new maps specifically for navigation that highlight traffic conditions and helpful landmarks. The calculations use the user's current location and compare it to the current route that the user's traversing to provide critical information at any given moment. You control the entire experience, from the time your user chooses a destination to when they arrive.<hr>

### The output for the sample source code
![gif (2)](https://user-images.githubusercontent.com/63853553/101982121-6bf7a300-3c7a-11eb-8cd4-2d5c0c2adb6c.gif)<hr>

### Techniques used in this project<br>
1-Mapbox<br>
2-Lifecycle Components<br>
3-Navigation Components<br>
4-Cloud Firestore<br>
5-Glide<br>
6-Dexter<br>
7-Timber<br>
8-RoundedImageView<br>
9-Material<br>
10-DataBinding<br>
11-Opencellid.org<hr>

### Getting Started

<div class="highlight highlight-source-groovy-gradle"><pre><span class="pl-en">repositories</span> {
    mavenCentral()
    maven { url <span class="pl-s"><span class="pl-pds">"</span>http://oss.sonatype.org/content/repositories/snapshots/<span class="pl-pds">"</span></span> }
}

<span class="pl-en">dependencies</span> {
    implementation <span class="pl-s"><span class="pl-pds">'</span>com.mapbox.mapboxsdk:mapbox-android-sdk:9.2.0<span class="pl-pds">'</span></span>
     implementation <span class="pl-s"><span class="pl-pds">'</span>com.mapbox.mapboxsdk:mapbox-android-navigation:0.42.6<span class="pl-pds">'</span></span>
      implementation <span class="pl-s"><span class="pl-pds">'</span>com.mapbox.mapboxsdk:mapbox-android-plugin-places-v9:0.12.0<span class="pl-pds">'</span></span>
       implementation <span class="pl-s"><span class="pl-pds">'</span>com.mapbox.mapboxsdk:mapbox-android-sdk:9.2.0<span class="pl-pds">'</span></span>
}</pre></div>
